package com.tribalyte.interview;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MixProblems {
    private static final Logger log = LoggerFactory.getLogger(MixProblems.class);

    /** 
     * TEST1: JUMP GAME
     * 
     * Given an array of non-negative integers, you are initially positioned at the first index of the array.
     * Each element in the array represents your maximum jump length at that position.
     * Determine if you are able to reach the last index.
     * For example:
     * int sequence[] = { 2, 3, 1, 1, 4 }; -> true
     * int sequence[] = { 3, 2, 1, 0, 4 }; -> false
     * int sequence[] = { 2, 2, 1, 2, 4 }; -> true
     */
    @Test
    public void jumpGame() {
        int sequence[] = { 2, 2, 1, 2, 4 };
        boolean result = canJump(sequence);
        log.info("result {}", result);
    }

    private boolean canJump(int sequence[]) {

        if (sequence.length <= 1) {
            return true;
        }

        int max = sequence[0];
        for (int i = 0; i < sequence.length; i++) {

            // target reached
            if (max >= sequence.length - 1) {
                return true;
            }

            // target failed
            if (sequence[i] == 0 && i == max) {
                return false;
            }

            // update max
            if (i + sequence[i] > max) {
                max = i + sequence[i];
            }
        }

        return false;
    }

    /**
     *     Rocks and towers on a chess.
     *     How many towers can we place on a chessboard without any of them being able to take each other?
     *     The anwswer is 8 for a regular chessboard, just put them on the diagonal.
     *     
     *     Now if there are forbidden squares on the chessboard where it is not allowed to put a tower?
     *     One possibility here is to brute force the problem which the candidate should be able to come up with.
     *     There are many pruning strategies that can work there too.
     *     One elegant solution is to formulate this as a flow problem (not many candidates can do that though)
     */
}
