package com.tribalyte.interview;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * Dynamic programming.
 * Dynamic programming is a method for solving a complex problem by breaking it down into a collection 
 * of simpler subproblems, solving each of those subproblems just once, and storing their solutions, 
 * ideally, using a memory-based data structure.
 * The next time the same subproblem occurs, instead of recomputing its solution, one simply looks up 
 * the previously computed solution, thereby saving computation time at the expense of a (hopefully) 
 * modest expenditure in storage space. 
 * (Each of the subproblem solutions is indexed in some way, typically based on the values of its 
 * input parameters, so as to facilitate its lookup.)
 * Dynamic programming algorithms are often used for optimization. A dynamic programming algorithm 
 * will examine the previously solved subproblems and will combine their solutions to give the best 
 * solution for the given problem.
 * 
 * Most of the dynamic programming problems can be solved by using Breadth-first search (BFS).
 * BFS is an algorithm for traversing or searching tree or graph data structures. 
 * It starts at the tree root and explores the neighbor nodes first, before moving to the next level neighbors.
 */
public class DynamicProgramming {
    private static final Logger log = LoggerFactory.getLogger(DynamicProgramming.class);

    /** 
     * TEST1: COIN CHANGE
     * 
     * You are given coins of different denominations and a total amount of money amount. 
     * Write a function to compute the fewest number of coins that you need to make up that amount. 
     * If that amount of money cannot be made up by any combination of the coins, return -1.
     */
    @Test
    public void coinChangeMain() {
        int coins[] = { 25, 1 };
        int amount = 17;
        int result = coinChange2(coins, amount);
        log.info("result {}", result);
    }

    /**
     * We use an array where:
     * - the index is the amount 
     * - value the minimum number of coins needed
     * Therefore dp[v] is the minimum number of coins required to get the amount v.
     * This solution takes time O(n^3).  
     */
    public int coinChange1(int[] coins, int amount) {
        if (amount == 0)
            return 0;

        // Initially set every dp[i] to MAX_VALUE.
        int[] dp = new int[amount + 1];
        dp[0] = 0; // do not need any coin to get 0 amount
        for (int i = 1; i <= amount; i++)
            dp[i] = Integer.MAX_VALUE;

        for (int i = 0; i <= amount; i++) {
            for (int coin : coins) {
                if (i + coin <= amount) {

                    if (dp[i] == Integer.MAX_VALUE) {
                        // if the amount 'i' is not reachable
                        // keep the previous value
                        dp[i + coin] = dp[i + coin];

                    } else {
                        // if the amount 'i' is reachable
                        // keep the minimum between the previous value 
                        // and the number of coins for the amount 'i' + 1
                        dp[i + coin] = Math.min(dp[i + coin], dp[i] + 1);
                    }
                }
            }
        }

        if (dp[amount] == Integer.MAX_VALUE)
            return -1;

        return dp[amount];
    }

    /**
     * Breath First Search (BFS) solution.
     * We can view this problem as going to a target position with steps that are allows 
     * in the array coins. 
     * We maintain two queues: 
     * - the amount reached so far (amountQueue)
     * - the minimal number of coins needed (stepQueue)
     * 
     * Because the "contains" method take n, total time is O(n^3).
     */
    public int coinChange2(int[] coins, int amount) {
        if (amount == 0)
            return 0;

        Queue<Integer> amountQueue = new ArrayBlockingQueue<>(amount);
        Queue<Integer> stepQueue = new ArrayBlockingQueue<>(amount);

        // to get amount 0, 0 step is required
        amountQueue.offer(0);
        stepQueue.offer(0);

        // Terminal condition we don't have any more elements in the queue
        // to evaluate
        while (amountQueue.size() > 0) {

            int tempAmount = amountQueue.poll();
            int step = stepQueue.poll();

            // exit with success
            if (tempAmount == amount)
                return step;

            // exit with failure
            if (tempAmount > amount) {
                continue;
            }

            // Compute the amount that we can reach with step + 1
            for (int coin : coins) {

                // Consider only the amount that were not reached before
                // because if there were reached before the number of steps
                // were less
                if (!amountQueue.contains(tempAmount + coin)) {
                    amountQueue.offer(tempAmount + coin);
                    stepQueue.offer(step + 1);
                }
            }
        }

        return -1;
    }

    /** Saurabh */
    //    int coinChange3(int[] coins, int sz, int amount) {
    //
    //        if (sz) {
    //            int max = coins[sz - 1];
    //            coinCount = amount / max;
    //            remaining = amount - max * cointCount;
    //            if (!remaining) {
    //                return coinCount;
    //            }
    //            int tmp = coinChange(coins, sz - 1, remaining);
    //            if (tmp >= 0) {
    //                return coinCount + tmp;
    //            }
    //            while (tmp < 0) {
    //                remaining = remaining + max;
    //                coinCount--;
    //                tmp = coinChange(coins, sz - 1, remaining);
    //                if (tmp >= 0) {
    //                    return coinCount + tmp;
    //                } else if (coinCount == 0) {
    //                    return -1;
    //                }
    //            }
    //        } else {
    //            return -1;
    //        }
    //    }

    /**
     * On an nxn board with squares colored in back or white, find the size of the biggest subsquare that has only 
     * white squares.
     * This is a dynamic programming problem. As for (almost) every dynamic programming problem its simplest 
     * formuation is a memoized recursion.
     * The challenge is to find the function that allows the recursion to be simple.
     * In this case one that works is:
     * "biggest_subsquare_with_upper_left_corner_in(i, j)"
     * With the recursive relationship:
     * biggest_subsquare_with_upper_left_corner_in(i, j) =
     * i == 0 || j == 0  — > return 0
     * chessboard[i, j] == 'Black' -> return 0
     * return 1 + min(
     * biggest_subsquare_with_upper_left_corner_in(i + 1, j),
     * biggest_subsquare_with_upper_left_corner_in(i , j + 1),
     * biggest_subsquare_with_upper_left_corner_in(i+ , j + 1),
     * )
     * Basically this is the idea… it needs memoization + boundaries checking at n of course… */

}
